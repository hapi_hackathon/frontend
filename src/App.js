import React, { Suspense } from 'react'
import { Route, Routes } from 'react-router-dom'
import { EuiThemeProvider } from '@elastic/eui'

import Layout from './extensions/Layout'

import Home from './pages/Home'
import Users from './pages/Users'
import Registration from './pages/Registration'
import Authorization from './pages/Authorization'

import { theme } from './constants/theme'

import { Routs } from './routs'

import './App.scss'

const App = () => {
	return (
		<>
			<Suspense fallback={<div>loadinf</div>}>
				<EuiThemeProvider theme={theme}>
					<div className="app">
						<Routes>
							<Route
								path={Routs.index}
								element={
									<Layout>
										<Home/>
									</Layout>
								}
							/>
							<Route
								path={Routs.users.index}
								element={
									<Layout>
										<Users/>
									</Layout>
								}
							/>
							<Route
								path={Routs.auth.registration.index}
								element={<Registration/>}
							/>
							<Route
								path={Routs.auth.authorization.index}
								element={<Authorization/>}
							/>
							<Route
								path="*"
								element={(
									<div>404</div>
								)}
							/>
						</Routes>
					</div>
				</EuiThemeProvider>
			</Suspense>
		</>
	)
}

export default App
