/**
 * Copyright NGR Softlab 2023
 */

import misc from '../index'

export const apiPostLogIn = async (data) => {
	const url = '/api/users/authenticate'
	const response = await misc.post(url, data)

	return response.data
}

export const apiPostLogOut = async (data) => {
	const url = '/api/users/authenticate'
	const response = await misc.post(url, data)

	return response.data
}
