import React, { memo } from 'react'
import { EuiText } from '@elastic/eui'

import { COLOR_DEFAULT } from '../../constants/colors'

const Logo = () => {
	return (
		<>
			<EuiText
				size="m"
				color={COLOR_DEFAULT}
				grow={false}
				onClick={null}
				className={null}
				style={null}
			>
				<b>Central Dynamics</b>
			</EuiText>
		</>
	)
}

export default memo(Logo)
