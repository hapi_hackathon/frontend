import React from 'react'
import { EuiFlexGroup, EuiFlexItem, EuiText } from '@elastic/eui'

import { COLOR_PRIMARY, COLOR_SUBDUED } from '../../constants/colors'

const User = () => {
	return (
		<>
			<EuiFlexGroup gutterSize="m" alignItems="center">
				<EuiFlexItem>
					<EuiText
						size="m"
						color={COLOR_PRIMARY}
						grow={false}
						onClick={null}
						className={null}
						style={null}
					>
						Дмитрий Губин
					</EuiText>
				</EuiFlexItem>
				<EuiFlexItem grow={false}>
					<EuiText
						size="m"
						color={COLOR_SUBDUED}
						grow={false}
						onClick={null}
						className={null}
						style={null}
					>
						Д
					</EuiText>
				</EuiFlexItem>
			</EuiFlexGroup>
		</>
	)
}

export default User
