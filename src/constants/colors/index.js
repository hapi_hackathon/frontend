export const COLOR_PRIMARY = 'primary' // primary
export const COLOR_SUCCESS = 'success' // success
export const COLOR_WARNING = 'warning' // warning
export const COLOR_DANGER = 'danger' // danger
export const COLOR_DEFAULT = 'default' // default
export const COLOR_SUBDUED = 'subdued' // subdued
