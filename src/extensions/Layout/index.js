/**
 * Copyright NGR Softlab 2023
 */

import React from 'react'
import { EuiFlexGroup, EuiFlexItem, EuiPage, EuiPanel } from '@elastic/eui'

import Header from '../../widgets/Header'
import Sidebar from '../../widgets/Sidebar'

import classes from './index.module.scss'

const Layout = ({ children }) => {
	return (
		<>
			<EuiPage paddingSize="m">
				<EuiFlexGroup gutterSize="m" direction="column">
					<EuiFlexItem>
						<Header/>
					</EuiFlexItem>
					<EuiFlexItem>
						<EuiPanel paddingSize="none">
							<EuiFlexGroup>
								<EuiFlexItem grow={false} className={classes.sidebar}>
									<Sidebar/>
								</EuiFlexItem>
								<EuiFlexItem>
									{children}
								</EuiFlexItem>
							</EuiFlexGroup>
						</EuiPanel>
					</EuiFlexItem>
				</EuiFlexGroup>
			</EuiPage>
		</>
	)
}

export default Layout
