/**
 * Copyright NGR Softlab 2023
 */

import { useCallback, useEffect, useLayoutEffect, useRef, useState } from 'react'
import _ from 'lodash'

export const useLocalStorageState = (key, initialValue) => {
	const [value, setValue] = useState(() => {
		const stored = window.localStorage.getItem(key)

		if (!_.isNil(stored)) {
			return JSON.parse(stored)
		}

		if (_.isFunction(initialValue)) {
			return initialValue()
		}

		return initialValue
	})

	const ref = useRef(value)

	useLayoutEffect(() => {
		ref.current = value
	}, [value])

	useEffect(() => {
		const stored = window.localStorage.getItem(key)

		if (_.isNil(stored)) {
			if (_.isNil(initialValue)) {
				window.localStorage.setItem(key, null)
			} else {
				window.localStorage.setItem(key, JSON.stringify(initialValue))
			}
		}
	}, [initialValue, key])

	const handleSetValue = useCallback((data) => {
		setValue(data)

		const actual = _.isFunction(data) ? data(ref.current) : data

		window.localStorage.setItem(key, JSON.stringify(actual))
	}, [key, ref])

	return [value, handleSetValue]
}
