export const Routs = {
	index: '/',
	auth: {
		authorization: {
			index: '/auth/sign-in'
		},
		registration: {
			index: '/auth/sign-up'
		}
	},
	settings: {
		index: '/settings',
		authentication: {
			index: '/settings/2fa'
		},
		change: {
			password: {
				index: '/settings/change/password'
			},
			name: {
				index: '/settings/change/name'
			}
		}
	},
	users: {
		index: '/users',
		add: {
			index: '/users/add'
		},
		remove: {
			index: '/users/remove/:id'
		},
		change: {
			password: {
				index: '/users/change/password'
			},
			name: {
				index: '/users/change/name'
			}
		}
	}
}
