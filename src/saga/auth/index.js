/**
 * Copyright NGR Softlab 2023
 */

import { call, put, takeEvery } from 'redux-saga/effects'

import { apiPostLogIn, apiPostLogOut } from '../../api/auth'

import { authActionTypes } from '../../redux/reducers/auth'

function* authWorker(action) {
	try {
		const data = yield call(apiPostLogIn, action.data)

		yield call()

		return yield put({
			type: authActionTypes.AUTH_RESPONSE_SUCCESS,
			data: data
		})
	} catch (error) {
		return yield put({
			type: authActionTypes.AUTH_RESPONSE_FAILURE,
			data: error.response.data
		})
	}
}

function* outWorker() {
	try {
		const data = yield call(apiPostLogOut)

		return yield put({
			type: authActionTypes.OUT_RESPONSE_SUCCESS,
			data: data
		})
	} catch (error) {
		return yield put({
			type: authActionTypes.OUT_RESPONSE_FAILURE,
			error: error.response.data
		})
	}
}

export function* watchAuthAsync() {
	yield takeEvery(authActionTypes.AUTH_REQUEST, authWorker)
	yield takeEvery(authActionTypes.OUT_REQUEST, outWorker)
}
