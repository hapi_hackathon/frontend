/**
 * Copyright NGR Softlab 2023
 */

import { all } from 'redux-saga/effects'

import { watchAuthAsync } from './auth'

export default function* sagas() {
	yield all([
		watchAuthAsync()
	])
}
