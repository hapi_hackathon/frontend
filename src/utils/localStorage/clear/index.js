/**
 * Copyright NGR Softlab 2023
 */

export const clear = () => {
	window.localStorage.clear()
}
