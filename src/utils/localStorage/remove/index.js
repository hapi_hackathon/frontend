/**
 * Copyright NGR Softlab 2023
 */

export const remove = (key) => {
	if (key) {
		window.localStorage.removeItem(key)
	}
}
