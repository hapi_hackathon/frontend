/**
 * Copyright NGR Softlab 2023
 */

import _ from 'lodash'

export const set = (key, value) => {
	if (key) {
		if (_.isUndefined(value)) {
			window.localStorage.setItem(key, null)
		}

		window.localStorage.setItem(key, JSON.stringify(value))
	}
}
