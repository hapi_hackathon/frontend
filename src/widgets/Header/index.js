import React, { memo } from 'react'
import { EuiFlexGroup, EuiFlexItem, EuiIcon } from '@elastic/eui'

import { COLOR_PRIMARY } from '../../constants/colors'

import User from '../../components/User'
import Settings from '../../components/Settings'
import Logo from '../../components/Logo'

const Header = () => {
	return (
		<>
			<EuiFlexGroup gutterSize="m" justifyContent="flexBetween">
				<EuiFlexItem>
					<EuiFlexGroup gutterSize="m" alignItems="center">
						<EuiFlexItem grow={false}>
							<Logo/>
						</EuiFlexItem>
						<EuiFlexItem grow={false}>
							|
						</EuiFlexItem>
						<EuiFlexItem>
							Управление проектом
						</EuiFlexItem>
					</EuiFlexGroup>
				</EuiFlexItem>
				<EuiFlexItem grow={false}>
					<EuiFlexGroup gutterSize="m" alignItems="center">
						<EuiFlexItem grow={false}>
							<EuiIcon
								aria-label="icon-bell"
								type="bell"
								size="m"
								color={COLOR_PRIMARY}
								onClick={null}
								style={null}
							/>
						</EuiFlexItem>
						<EuiFlexItem grow={false}>
							<User/>
						</EuiFlexItem>
						<EuiFlexItem grow={false}>
							<Settings/>
						</EuiFlexItem>
					</EuiFlexGroup>
				</EuiFlexItem>
			</EuiFlexGroup>
		</>
	)
}

export default memo(Header)
