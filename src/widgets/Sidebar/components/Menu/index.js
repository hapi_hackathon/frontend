import React, { memo } from 'react'
import { EuiFlexGroup, EuiFlexItem, EuiIcon, EuiText } from '@elastic/eui'
import { Link, useLocation } from 'react-router-dom'

import { COLOR_DEFAULT, COLOR_SUBDUED } from '../../../../constants/colors'

import { Routs } from '../../../../routs'

const Menu = () => {

	const { pathname } = useLocation()

	return (
		<>
			<EuiFlexGroup gutterSize="m" direction="column">
				<EuiFlexItem>
					<EuiFlexGroup gutterSize="s" alignItems="center">
						<EuiFlexItem grow={false}>
							<EuiIcon
								aria-label="icon-apps"
								type="apps"
								size="m"
								color={pathname === Routs.index ? COLOR_DEFAULT : COLOR_SUBDUED}
								onClick={null}
								style={null}
							/>
						</EuiFlexItem>
						<EuiFlexItem>
							<Link to={Routs.index}>
								<EuiText
									size="m"
									color={pathname === Routs.index ? COLOR_DEFAULT : COLOR_SUBDUED}
									grow={false}
									onClick={null}
									className={null}
									style={null}
								>
									Главная
								</EuiText>
							</Link>
						</EuiFlexItem>
					</EuiFlexGroup>
				</EuiFlexItem>
				<EuiFlexItem>
					<EuiFlexGroup gutterSize="s" alignItems="center">
						<EuiFlexItem grow={false}>
							<EuiIcon
								aria-label="icon-users"
								type="users"
								size="m"
								color={pathname === Routs.users.index ? COLOR_DEFAULT : COLOR_SUBDUED}
								onClick={null}
								style={null}
							/>
						</EuiFlexItem>
						<EuiFlexItem>
							<Link to={Routs.users.index}>
								<EuiText
									size="m"
									color={pathname === Routs.users.index ? COLOR_DEFAULT : COLOR_SUBDUED}
									grow={false}
									onClick={null}
									className={null}
									style={null}
								>
									Пользователи
								</EuiText>
							</Link>
						</EuiFlexItem>
					</EuiFlexGroup>
				</EuiFlexItem>
			</EuiFlexGroup>
		</>
	)
}

export default memo(Menu)
